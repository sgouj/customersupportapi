package com.allstate.crm.dao;

import com.allstate.crm.entities.Customer;
import com.allstate.crm.entities.Policy;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class CustomerDAOITTest {

    @Autowired
    CustomerDAO custDao;

    @Autowired
    MongoTemplate mongoTemplate;

    private Customer custEntitiy;

    @BeforeEach
    public void cleanupAndReset() {
        custEntitiy = new Customer();
        custEntitiy.setFirstName("test");
        custEntitiy.setLastName("zzzz");
        custEntitiy.setAddress("Wheeling 123 789");
        custEntitiy.setAge(20);
        custEntitiy.setCity("Chicago");
        custEntitiy.setState("IL");
        custEntitiy.setEmail("test@test.com");
        custEntitiy.setPhone("23498657");
        custEntitiy.setSsn("6783423449");
        custEntitiy.setGender("M");
        custEntitiy.setLanguage("English");
        mongoTemplate.dropCollection(Customer.class);
        mongoTemplate.insert(custEntitiy);
    }

    @Test
    public void saveCustomerSuccess(){
        long custId = custDao.saveCustomer(custEntitiy);
        assertEquals(3, custId);
    }


}
