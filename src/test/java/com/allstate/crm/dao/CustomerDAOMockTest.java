package com.allstate.crm.dao;

import com.allstate.crm.dao.common.DatabaseGenerateCounter;
import com.allstate.crm.entities.Customer;
import com.allstate.crm.entities.Policy;
import org.junit.After;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.doReturn;

@SpringBootTest
public class CustomerDAOMockTest {
    @Mock
    private CustomerDAO dao;

    @Mock
    private DatabaseGenerateCounter dbCounter;

    private Customer custEntitiy;

    private AutoCloseable closeable;

    @BeforeEach
    public  void openMocks() {
        closeable = MockitoAnnotations.openMocks(this);
        custEntitiy = new  Customer();
        custEntitiy.setFirstName("test");
        custEntitiy.setLastName("zzzz");
        custEntitiy.setAddress("Wheeling 123 789");
        custEntitiy.setAge(20);
        custEntitiy.setCity("Chicago");
        custEntitiy.setState("IL");
        custEntitiy.setEmail("test@test.com");
        custEntitiy.setPhone("23498657");
        custEntitiy.setSsn("6783423449");
        custEntitiy.setGender("M");
        custEntitiy.setLanguage("English");

    }

    @After
    public void releaseMocks() throws Exception {
        closeable.close();
    }

    @Test
    void saveCustomerSuccess(){
        doReturn(3L).when(dbCounter).generateSequence(anyString());
        doReturn(3L).when(dao).saveCustomer(any(Customer.class));
        assertEquals(3L, dao.saveCustomer(custEntitiy));
    }

    @Test
    void updateCustomerSuccess(){
        custEntitiy.setId(3);
        doReturn(1L).when(dao).updateCustomer(any(Customer.class));
        assertEquals(1L, dao.updateCustomer(custEntitiy));
    }

}
