package com.allstate.crm.entities;

import org.junit.jupiter.api.Test;

import java.util.Date;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class PolicyEntityTest {

     long policyId = 1;
     long custId  = 21;
     String policyNo = "9876541344";
     double amount = 3287.47;
     Date effectiveDate = new Date();
     String productType = "Auto";
     String company= "Allstate";
     String status= "Active";

    @Test
    public void constructorInitializesPaymentObjectWithExpectedValues() {

        Policy policy = new Policy(policyId,
                custId,
                policyNo,
                amount,
                effectiveDate,
                productType,
                company,
                status,
                null,
                null);

        assertEquals(policyId, policy.getId());
        assertEquals(effectiveDate, policy.getEffectiveDate());
        assertEquals(productType, policy.getProductType());
        assertEquals(amount, policy.getPremiumAmount());
        assertEquals(custId, policy.getCustId());
    }

}
