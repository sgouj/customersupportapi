package com.allstate.crm.service;

import com.allstate.crm.dao.PolicyDAO;
import com.allstate.crm.entities.Policy;
import com.allstate.crm.exception.PolicyNotFoundException;
import com.allstate.crm.exception.PolicyServiceException;
import com.allstate.crm.utility.ValidationHelper;
import org.junit.After;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@SpringBootTest
public class PolicyServiceMockTest {

    @Autowired
    PolicyService policyService;

    @MockBean
    PolicyDAO policyDAO;

    @MockBean
    ValidationHelper validation;

    private AutoCloseable closeable;
    private Policy policyEntity;

    @BeforeEach
    public  void openMocks() {
        closeable = MockitoAnnotations.openMocks(this);
        policyEntity = new Policy(
                1,
                21,
                "9876541344",
                3287.47,
                new Date(),
                "Auto",
                "Allstate",
                "Active",
                null,
                null
        );
    }

    @Test
    public void getPolicyByCustomerIdSuccess()
    {
        List<Policy> lstPolicy = new ArrayList<>();
        lstPolicy.add(policyEntity);
        doReturn(lstPolicy).when(policyDAO).getPolicyByCustomerId(21);
        assertEquals(1, policyService.getPolicyDetailByCustId(21).size());
        assertEquals(policyEntity.getId(), lstPolicy.get(0).getId());
        assertEquals(policyEntity.getCustId(), lstPolicy.get(0).getCustId());
        assertEquals(policyEntity.getPremiumAmount(), lstPolicy.get(0).getPremiumAmount());
        assertEquals(policyEntity.getCompany(), lstPolicy.get(0).getCompany());
    }

    @Test
    public void getPolicyByCustomerIdFailure()
    {
        doReturn(false).when(validation).validateIntegerGreaterThanZero(0);
        PolicyServiceException exception = assertThrows(PolicyServiceException.class, () -> {
            policyService.getPolicyDetailByCustId(0);
        });

        String expectedMessage = "Policy Information not found.";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    public void getPolicyByCustomerIdFailureTypePolicyService()
    {
        doReturn(true).when(validation).validateIntegerGreaterThanZero(0);
        doThrow(new RuntimeException()).when(policyDAO).getPolicyByCustomerId(anyInt());
        PolicyNotFoundException exception = assertThrows(PolicyNotFoundException.class, () -> {
            policyService.getPolicyDetailByCustId(0);
        });

        String expectedMessage = "Error occured while processing your request.";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));
    }

    @After
    public void releaseMocks() throws Exception {
        closeable.close();
    }
}
