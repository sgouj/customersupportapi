package com.allstate.crm.dao;

import com.allstate.crm.entities.Customer;
import com.allstate.crm.entities.Policy;
import com.mongodb.client.result.UpdateResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;
import com.allstate.crm.dao.common.DatabaseGenerateCounter;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class PolicyDAOImpl implements PolicyDAO{
    @Autowired
    private MongoTemplate mongoTemp;

    public PolicyDAOImpl() {
    }

    @Autowired
    private DatabaseGenerateCounter dbcounter;

    @Override
    public List<Policy> getPolicyByCustomerId(int custId) {
        List<Policy> policyDetalils = null;
        Query detailQuery = new Query();
        detailQuery.addCriteria(Criteria.where("custId").is(custId));
        policyDetalils = mongoTemp.find(detailQuery, Policy.class);
        return policyDetalils;
    }

    @Override
    public void savePolicy(List<Policy> policyList) {
        if(policyList.size() > 0) {
            long idCounter = dbcounter.generateSequence(Policy.SEQUENCE_NAME);
            for (Policy p : policyList) {
                p.setId(idCounter);
                idCounter = idCounter + 1;
            }
            mongoTemp.insertAll(policyList);
            dbcounter.updateSequence(idCounter-1, Policy.SEQUENCE_NAME);
        }
    }

    @Override
    public int updatePolicy(List<Policy> policies) {
        int countOfAffectedRows = 0;
        for (Policy policy: policies) {
            Query q = new Query();
            q.addCriteria(Criteria.where("id").is(policy.getId()));
            Update u = new Update();
            u.set("premiumAmount", policy.getPremiumAmount());
            u.set("effectiveDate", policy.getEffectiveDate());
            u.set("status", policy.getStatus());
            u.set("terminationDate", policy.getTerminationDate());
            u.set("terminationReason", policy.getTerminationReason());
            UpdateResult ur = mongoTemp.updateFirst(q,u, Policy.class);
            countOfAffectedRows += ur.getModifiedCount();
        }
        return countOfAffectedRows;
    }
}
