package com.allstate.crm.dao;

import com.allstate.crm.entities.Policy;
import java.util.List;

public interface PolicyDAO {
    //function for getting policy info by customer id
   List<Policy> getPolicyByCustomerId(int custId);

    void savePolicy(List<Policy> policylist);

    int updatePolicy(List<Policy> policies);
}
