package com.allstate.crm.dao;

import com.allstate.crm.dao.common.DatabaseGenerateCounter;
import com.allstate.crm.entities.Customer;
import com.allstate.crm.entities.Policy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.data.mongodb.core.query.Query;
import com.mongodb.client.result.UpdateResult;

import java.util.List;


@Repository
public class CustomerDAOImpl implements CustomerDAO {

    public CustomerDAOImpl()
    {

    }

    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private DatabaseGenerateCounter dbcounter;

    @Override
    public long saveCustomer(Customer customer) {
        customer.setId(dbcounter.generateSequence(Customer.SEQUENCE_NAME));
        Customer customerRes = mongoTemplate.insert(customer);
        if(customerRes != null)
        {
            return customerRes.getId();
        }
        else
        {
            return 0;
        }
    }

    @Override
    public long updateCustomer(Customer customer) {
        Query q = new Query();
        q.addCriteria(Criteria.where("id").is(customer.getId()));
        Update u = new Update();
        u.set("email", customer.getEmail());
        u.set("firstName", customer.getFirstName());
        u.set("age", customer.getAge());
        u.set("lastName", customer.getLastName());
        u.set("address", customer.getAddress());
        u.set("city", customer.getCity());
        u.set("state", customer.getState());
        u.set("phone", customer.getPhone());
        u.set("gender", customer.getGender());
        u.set("language", customer.getLanguage());
        UpdateResult ur = mongoTemplate.updateFirst(q,u,Customer.class);
        return ur.getMatchedCount();
    }

    @Override
    public Customer getCustomerById(long id) {
        Customer customer;
        Query detailQuery = new Query();
        detailQuery.addCriteria(Criteria.where("id").is(id));
        customer = mongoTemplate.findOne(detailQuery, Customer.class);
        return customer;
    }
}
