package com.allstate.crm.dao;

import com.allstate.crm.entities.Customer;

public interface CustomerDAO {

    long saveCustomer(Customer customer);

    long updateCustomer(Customer customer);

    Customer getCustomerById(long id);
}
