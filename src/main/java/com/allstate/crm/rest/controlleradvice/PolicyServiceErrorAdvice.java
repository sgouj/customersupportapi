package com.allstate.crm.rest.controlleradvice;

import com.allstate.crm.exception.PolicyNotFoundException;
import com.allstate.crm.exception.PolicyServiceException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import static org.springframework.http.HttpStatus.BAD_REQUEST;

@ControllerAdvice
public class PolicyServiceErrorAdvice {

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(value = PolicyNotFoundException.class)
    public ResponseEntity<Object> exception(PolicyNotFoundException e){
        return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
    }

    @ResponseStatus(BAD_REQUEST)
    @ExceptionHandler({PolicyServiceException.class})
    public ResponseEntity<Object> exception(PolicyServiceException e) {
        return new ResponseEntity<>(e.getMessage(), BAD_REQUEST);
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<Object> exception(MethodArgumentNotValidException e) {
        //System.out.println(e.getMessage());
        return new ResponseEntity<>("Record not saved, please enter correct details.", BAD_REQUEST);
    }
}
