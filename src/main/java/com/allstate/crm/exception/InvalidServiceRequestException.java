package com.allstate.crm.exception;

public class InvalidServiceRequestException extends RuntimeException {
    public InvalidServiceRequestException(String message) {
        super(message);
    }
}
