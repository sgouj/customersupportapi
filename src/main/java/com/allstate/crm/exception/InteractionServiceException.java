package com.allstate.crm.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class  InteractionServiceException extends RuntimeException{
    public  InteractionServiceException(String message)
    {
        super(message);
    }
}
