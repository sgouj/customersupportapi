package com.allstate.crm.search.repository;

import com.allstate.crm.entities.Customer;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface CustomerMongoRepository extends MongoRepository<Customer, Long> {
    public List<Customer> findByFirstNameIgnoreCase(String firstName);
    public List<Customer> findByPhone(String phoneNumber);
    public List<Customer> findByEmailIgnoreCase(String email);
    public List<Customer> findByPoliciesIn(List<String> policies);
}
