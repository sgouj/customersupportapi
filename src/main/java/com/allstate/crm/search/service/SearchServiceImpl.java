package com.allstate.crm.search.service;

import com.allstate.crm.dao.PolicyDAO;
import com.allstate.crm.entities.Customer;
import com.allstate.crm.entities.Policy;
import com.allstate.crm.exception.InvalidServiceRequestException;
import com.allstate.crm.exception.PolicyNotFoundException;
import com.allstate.crm.exception.PolicyServiceException;
import com.allstate.crm.exception.SearchServiceException;
import com.allstate.crm.search.dao.SearchDAO;
import com.allstate.crm.utility.ValidationHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.ArrayList;
import java.util.List;

@Service
public class SearchServiceImpl implements SearchService {

    @Autowired
    private SearchDAO searchDAO;

    @Autowired
    private ValidationHelper validationHelper;

    public SearchServiceImpl() {
    }


    @Override
    public List<Customer> getCustomersByFirstName(String firstName) {
        List<Customer> customers = new ArrayList<Customer>();
        try {
            if (validationHelper.isEmptyString(firstName)) {
                throw new InvalidServiceRequestException("First Name Cannot be Null/Empty");
            } else {
                customers = searchDAO.findCustomersByFirstName(firstName);
            }
        }catch (Exception exception) {
            throw new SearchServiceException(exception.getMessage());
        }
        return customers;
    }

    @Override
    public List<Customer> getCustomersByPolicyNumber(String policyNumber) {
        List<Customer> customers = new ArrayList<Customer>();
        try {
            if (validationHelper.isEmptyString(policyNumber)) {
                throw new InvalidServiceRequestException("PolicyNumber Cannot be Null/Empty");
            } else {
                customers = searchDAO.findCustomersByPolicyNumber(policyNumber);
            }
        }catch (Exception exception) {
            throw new SearchServiceException(exception.getMessage());
        }
        return customers;
    }

    @Override
    public List<Customer> getCustomersByPhoneNumber(String phoneNumber) {
        List<Customer> customers = new ArrayList<Customer>();
        try {
            if (validationHelper.isEmptyString(phoneNumber)) {
                throw new InvalidServiceRequestException("PhoneNumber Cannot be Null/Empty");
            } else {
                customers = searchDAO.findCustomersByPhoneNumber(phoneNumber);
            }
        }catch (Exception exception) {
            throw new SearchServiceException(exception.getMessage());
        }
        return customers;
    }

    @Override
    public List<Customer> getCustomersByEmail(String email) {
        List<Customer> customers = new ArrayList<Customer>();
        try {
            if (validationHelper.isEmptyString(email) || !validationHelper.isValidEmail(email)) {
                throw new InvalidServiceRequestException("Invalid/Empty Email id");
            } else {
                customers = searchDAO.findCustomersByEmail(email);
            }
        }catch (Exception exception) {
            throw new SearchServiceException(exception.getMessage());
        }
        return customers;
    }

    @Override
    public int saveCustomer(Customer customer) {
        searchDAO.saveCustomer(customer);
        return 1;
    }
}
