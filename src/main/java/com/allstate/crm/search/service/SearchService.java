package com.allstate.crm.search.service;

import com.allstate.crm.entities.Customer;
import com.allstate.crm.entities.Policy;

import java.util.List;

public interface SearchService {
    public List<Customer> getCustomersByFirstName(String firstName);
    public List<Customer> getCustomersByPolicyNumber(String policyNumber);
    public List<Customer> getCustomersByPhoneNumber(String phoneNumber);
    public List<Customer> getCustomersByEmail(String email);
    public int saveCustomer(Customer customer);
}
