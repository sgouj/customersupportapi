package com.allstate.crm.search.dao;

import com.allstate.crm.entities.Customer;

import java.util.List;

public interface SearchDAO {
    public List<Customer> findCustomersByFirstName(String firstName);
    public List<Customer> findCustomersByPolicyNumber(String policyNumber);
    public List<Customer> findCustomersByPhoneNumber(String phoneNumber);
    public List<Customer> findCustomersByEmail(String email);
    public int saveCustomer(Customer customer);
}
