package com.allstate.crm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@SpringBootApplication
@EnableMongoRepositories(basePackages = "com.allstate.crm.search.repository")
public class CustomersupportapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(CustomersupportapiApplication.class, args);
	}

}
