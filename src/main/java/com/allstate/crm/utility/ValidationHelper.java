package com.allstate.crm.utility;

import com.allstate.crm.AppConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


import java.util.function.IntPredicate;
import java.util.function.Predicate;
import java.util.regex.Pattern;

@Component
public class ValidationHelper {

    public ValidationHelper()
    {

    }
    @Autowired
    private AppConfig appConfig;

    public  boolean isEmptyString(String stringVal) {
        if(stringVal != null && !stringVal.trim().isEmpty())
            return false;
        return true;
    }

    public boolean validateStringPatterenifNotEmpty(String stringVal)
    {
        Pattern pattern = Pattern.compile(appConfig.getValidStringFormat());
        Predicate<String> checknotEmpty = (val) -> (val !=null && !stringVal.trim().isEmpty());
        Predicate<String> validatePattern = (val) -> (pattern.matcher(val).matches());
        return checknotEmpty.and(validatePattern).test(stringVal);
    }

    public  boolean validateIntegerGreaterThanZero(int number) {
        IntPredicate pred = (num) -> num > 0 ;
        return pred.test(number);
    }

    public boolean isValidEmail(String email) {
        Pattern pattern = Pattern.compile(appConfig.getEmailRegxPattern());
        return pattern.matcher(email).matches();
    }

}
