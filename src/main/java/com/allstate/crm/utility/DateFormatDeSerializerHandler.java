package com.allstate.crm.utility;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import org.springframework.beans.factory.annotation.Autowired;
import java.io.IOException;
import java.util.Date;

public class DateFormatDeSerializerHandler extends StdDeserializer<Date> {

    @Autowired
    DateTimeHelper dateHelper;

    public DateFormatDeSerializerHandler() {
        this(null);
    }
    public DateFormatDeSerializerHandler(Class<?> dt) {
        super(dt);
    }

    @Override
    public Date deserialize(JsonParser jsonP, DeserializationContext context) throws IOException
    {
        String actualDate = jsonP.getText();
        System.out.println(actualDate);
        try
        {
            //System.out.println(dateHelper.convertStringToDate(actualDate));
            return dateHelper.convertStringToDate(actualDate);
        }
        catch(Exception e) {
            return null;
        }
    }
}
