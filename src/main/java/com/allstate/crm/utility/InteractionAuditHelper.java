package com.allstate.crm.utility;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class InteractionAuditHelper {

    public InteractionAuditHelper(){

    }

    @Autowired
    ValidationHelper val;

    public String buildCreateAuditInteraction(String policy, String type, String name, String language, String preLanguage)
    {
        StringBuilder sb = new StringBuilder();
        sb.append(type).append(" for ").append(name).append(" through IVR/VOIP. ");
        if(!val.isEmptyString(policy) && type.indexOf("Create") >= 0)
        {
            sb.append("Created policy - , ").append(policy);
        }
        if(type.indexOf("Update") >= 0 && !val.isEmptyString(language) ) {

            sb.append("Updated language from ").append(preLanguage).append(" to ").append(language);
        }

        System.out.println("in audit helper val out");

        return sb.toString();
    }
}
