package com.allstate.crm.utility;

import com.allstate.crm.AppConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.text.DateFormat;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

@Component
public class DateTimeHelper {
    public DateTimeHelper()
    {

    }
    @Autowired
    private AppConfig appConfig;

    public  String convertDateToString(Date todayDate)
    {
        DateFormat todayDateFormat = new SimpleDateFormat(appConfig.getDateformat());
        todayDateFormat.setTimeZone(TimeZone.getTimeZone(appConfig.getTimeZone()));
        todayDateFormat.setLenient(false);
        return todayDateFormat.format(todayDate);
    }

    public  Date convertStringToDate(String todayDateString)
    {
        DateFormat todayDateFormat = new SimpleDateFormat(appConfig.getDateformat());
        todayDateFormat.setLenient(false);
        todayDateFormat.setTimeZone(TimeZone.getTimeZone(appConfig.getTimeZone()));
        ParsePosition pp = new ParsePosition(0);
        return todayDateFormat.parse(todayDateString, pp);
    }
}
