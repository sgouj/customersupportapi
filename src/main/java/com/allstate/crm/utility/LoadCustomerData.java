package com.allstate.crm.utility;

import com.allstate.crm.entities.Customer;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class LoadCustomerData {
    static List<Customer> customers= new ArrayList<Customer>();
    static {
        Customer customer = new Customer();
        customer.setId(12345);
        customer.setFirstName("Anil");
        customer.setLanguage("English");
        customer.setLastName("Illa");
        customer.setEmail("ailla@allstate1.com");
        customer.setAge(34);
        customer.setCity("bangalore");
        customer.setGender("male");
        customer.setPhone("8971234155");
        customer.setAddress("c 306, vaishno singature , hoodi 560048");
        customer.setSsn("123-45622-283464");
        customer.setState("karnataka");
        List<String> policies = new ArrayList<String>();
        policies.add("78923456712");
        customer.setPolicies(policies);
        customers.add(customer);
        Customer customer1 = new Customer();
        customer1.setId(6789);
        customer1.setFirstName("Sujal");
        customer1.setLanguage("Spanish");
        customer1.setLastName("Mitra");
        customer1.setEmail("sujal@allstate1.com");
        customer1.setAge(33);
        customer1.setCity("bangalore");
        customer1.setGender("male");
        customer1.setPhone("987654321");
        customer1.setAddress("c 306,  shoba apartment, marthahalli 560036");
        customer1.setSsn("3562-45622-283464");
        customer1.setState("karnataka");
        List<String> policies1 = new ArrayList<String>();
        policies1.add("9824563124");
        customer1.setPolicies(policies1);
        customers.add(customer1);
    }

    public static List<Customer> getCustomersByPhone(String phoneNumber) {
        return customers
                .stream()
                .filter(customer -> customer.getPhone().equalsIgnoreCase(phoneNumber))
                .collect(Collectors.toList());

    }
    public static List<Customer> getCustomersByFirstName(String firstName) {
        return customers
                .stream()
                .filter(customer -> customer.getFirstName().equalsIgnoreCase(firstName))
                .collect(Collectors.toList());

    }
    public static List<Customer> getCustomersByPolicyNumber(String policyNumber) {
        return customers
                .stream()
                .filter(customer -> customer.getPolicies().contains(policyNumber))
                .collect(Collectors.toList());

    }
    public static List<Customer> getCustomersByEmail(String email) {
        return customers
                .stream()
                .filter(customer -> customer.getEmail().equalsIgnoreCase(email))
                .collect(Collectors.toList());

    }

}
