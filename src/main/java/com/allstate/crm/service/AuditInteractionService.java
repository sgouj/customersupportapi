package com.allstate.crm.service;

import com.allstate.crm.entities.Customer;

public interface AuditInteractionService {

    long auditInteractions(Customer customer, String auditType, String policy, String language);
}
