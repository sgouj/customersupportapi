package com.allstate.crm.service;

import com.allstate.crm.entities.Interaction;
import com.allstate.crm.entities.Policy;

import java.util.List;

public interface InteractionService {
    List<Interaction> getInteractionDetailByCustId(int custId) ;

}
