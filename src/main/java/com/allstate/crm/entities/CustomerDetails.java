package com.allstate.crm.entities;

import javax.validation.Valid;
import java.util.List;

public class CustomerDetails {

    public CustomerDetails(){

    }

    public CustomerDetails(@Valid Customer customer, @Valid List<Policy> policies, @Valid Interaction interactons) {
        this.customer = customer;
        this.policies = policies;
        this.interactons = interactons;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public List<Policy> getPolicies() {
        return policies;
    }

    public void setPolicies(List<Policy> policies) {
        this.policies = policies;
    }

    public Interaction getInteractons() {
        return interactons;
    }

    public void setInteractons(Interaction interactons) {
        this.interactons = interactons;
    }

    @Valid
    private Customer customer;
    @Valid
    private List<Policy> policies;
    @Valid
    private Interaction interactons;
}
