package com.allstate.crm.entities;

import com.allstate.crm.utility.DateFormatDeSerializerHandler;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.Date;

@Document
public class Interaction {
    @Transient
    public static final String SEQUENCE_NAME = "interaction_sequence";

    @Id
    private long id;
    private long custId;

    @NotEmpty(message = "enter notes")
    @Pattern(regexp="^[\\w\\-\\s]+$", message="not valid interaction notes")
    @Size(max= 150)
    private String notes;

    @NotEmpty(message = "enter phone number")
    @Pattern(regexp="^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$", message="Mobile number is invalid")
    private String phone;

    @NotEmpty(message="Enter Valid policy number")
    @Pattern(regexp="^[a-zA-Z0-9]+$", message="not valid policy number")
    @Size(max = 10)
    private String policyNumber;

    private String createdBy;

    @JsonDeserialize(using = DateFormatDeSerializerHandler.class)
    private Date createdDate;

    public Interaction()
    {

    }
    public Interaction(long id, long custId, String notes, String phone, String policyNumber, String createdBy, Date createdDate) {
        this.id = id;
        this.custId = custId;
        this.notes = notes;
        this.phone = phone;
        this.policyNumber = policyNumber;
        this.createdBy = createdBy;
        this.createdDate = createdDate;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getCustId() {
        return custId;
    }

    public void setCustId(long custId) {
        this.custId = custId;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPolicyNumber() {
        return policyNumber;
    }

    public void setPolicyNumber(String policyNumber) {
        this.policyNumber = policyNumber;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }


}

